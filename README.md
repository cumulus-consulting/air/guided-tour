# Welcome to CITC

Cumulus in the Cloud provides you a full datacenter fabric with all of the features and functionality of Cumulus Linux running on real networking hardware.

Your lab consists of:

* 6 Leaf switches
* 4 Spine switches
* 8 Ubuntu servers
* 2 \"firewall\" devices to isolate tenants
* Complete out of band management networking

You can view the topology in the \"Nodes\" section below.

## Accessing the Lab

To access the lab, click on the node below to connect to the console.

The username and password to access the `oob-mgmt-server` is:

**Username**: `ubuntu`

**Password**: `nvidia`

The username and password to access each of the Cumulus Linux nodes is:

**Username**: `cumulus`

**Password**: `cumulus`

The username and password to access each of the Ubuntu nodes is:

**Username**: `ubuntu`

**Password**: `nvidia`

You can access any other device in the network from the OOB server via SSH to the device hostname. For example `ssh leaf01` or `ssh server06`.

## Provisioning the Lab

Currently your lab is unconfigured. If you know what you're doing, then feel free to use the `Accessing the Lab` section to log into the switches and configure them. For some more assistance, on the bar to the left, under the section **Learn** you can use our self-paced video on demand training by clicking **Cumulus Linux On Demand**. Those videos will help provide basics for configuration assistance.

For an approach that is more automated, use the **Demo Marketplace** option. In the **Learn** section on the left, click on the **Demo Marketplace** link and select **Cumulus VXLAN EVPN Symmetric** and click **Launch**. That demo will use the same topology as CITC but will be fully configured.

The last option is to use the `Production Ready Automation` solution to fully provision the network. This automation code is built with CITC as the base: https://gitlab.com/cumulus-consulting/goldenturtle/cumulus_ansible_modules/

## What's Next

From here you can continue to explore the lab. Break things, configure anything you'd like or try a different demo environment.

If you'd like to learn more you can try our self-paced [virtual test drive](https://cumulusnetworks.com/lp/cumulus-linux-on-demand/) course that will teach you the basics of Cumulus Linux with additional hands on labs.

You can also use the NetQ graphical interface in the menu on the left, which provides access to the NetQ data we've already seen and so much more!

If you want to get more involved, join our [Community Slack](http://slack.cumulusnetworks.com). If you have any questions or want more information about Cumulus Linux, Cumulus NetQ or anything else, please contact us by emailing [networking@nvidia.com](mailto:networking@nvidia.com).

